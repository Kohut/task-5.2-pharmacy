use triger_fk_cursor
go

create or alter trigger postUD
on post
after delete, update
as
if @@ROWCOUNT=0 return
begin
declare @del int = (select count(*) from deleted)
declare @up int= (select count(*) from inserted)
if @up = 0 
delete employee
where post in (select *from deleted)
else
update employee
set post = (select *from inserted)
where post in (select *from deleted)
end 
go

use triger_fk_cursor
go
create or alter trigger street_UD
on
street
after delete, update
as 
if @@ROWCOUNT = 0 return
begin
declare @del int = (select count (*) from deleted)
declare @up int = (select count (*)from inserted)
if @up = 0

use triger_fk_cursor
go

create or alter trigger zone_UD
on zone
after delete, update
as
if @@ROWCOUNT = 0 return
begin
declare @del int = (select count(*) from deleted)
declare @up int = (select count(*) from inserted)
if @del > 0
if (select count(*) from medicine_zone where zone_id in (select id from deleted))>0
print 'The statment has been terminated'
rollback
end
go

delete pharmacy
where street in (select *from deleted)
else
update pharmacy
set street = (select *from inserted)
where street in (select *from deleted)

end
go
