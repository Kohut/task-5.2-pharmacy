USE triger_fk_cursor
GO 

CREATE OR ALTER TRIGGER Medicine_pattern
ON medicine
AFTER INSERT, UPDATE
AS
IF @@ROWCOUNT=0 return
BEGIN
DECLARE @pattern int = (SELECT COUNT(*) FROM inserted WHERE ministry_code LIKE '[MP%]')
+ (SELECT COUNT(*) FROM inserted WHERE ministry_code LIKE '[AZ][MP]%')
+(SELECT COUNT(*) FROM inserted WHERE ministry_code NOT LIKE '[AZ][AZ][0-9][0-9][0-9[0-9[0-9]')
IF @pattern >0
BEGIN 
ROLLBACK
PRINT 'MINISTERY CODE IS WRONG, PLEASE CHECK!'
END
END

GO
use triger_fk_cursor
go
CREATE OR ALTER TRIGGER MEDICINE_I_U_D
ON medicine
AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
IF @@ROWCOUNT = 0 RETURN
BEGIN
declare @del int = (SELECT COUNT(*) from deleted)
declare @p int = (SELECT COUNT(*) from inserted)
IF @del >0
BEGIN
IF ( SELECT count(*) FROM medicine_zone WHERE medicine_id in (select id from deleted))>0
or
(select COUNT(*) FROM pharmacy_medicine WHERE medicine_id IN (select ID FROM deleted))> 0
ROLLBACK
PRINT 'THE STATEMENT HAS BEEN TERMINATED'
END
END 
END
GO
USE triger_fk_cursor
GO
CREATE OR ALTER TRIGGER medicine_zone_IUD
on
medicine_zone
after insert, UPDATE, DELETE
as
BEGIN
IF @@ROWCOUNT = 0  RETURN
DECLARE @zone int = (SELECT COUNT(*) from inserted where zone_id not in (SELECT id from [zone]))
DECLARE @medicine int = (SELECT COUNT(*) FROM inserted WHERE medicine_id NOT IN (select ID FROM medicine))
IF @zone>0 or @medicine >0 
begin
rollback
PRINT 'INCORECT FOREIGN KEY '
END
END
GO

use triger_fk_cursor
go

CREATE OR ALTER TRIGGER phamnacy_medicineINS
on
pharmacy_medicine
AFTER INSERT, UPDATE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @PHARMACY INT = (SELECT COUNT(*) FROM inserted WHERE pharmacy_id not in (select id from pharmacy))
declare @medicine int = (select count(*) from inserted where medicine_id not in (select id from medicine))
if
@PHARMACY >0 OR @medicine >0
BEGIN
ROLLBACK
PRINT 'INCORECT FOREIGN KEY'
END
END
GO

USE triger_fk_cursor
GO

CREATE OR ALTER TRIGGER post_restrict
ON
post
AFTER DELETE, UPDATE
AS
IF @@ROWCOUNT = 0 RETURN
ELSE
BEGIN
ROLLBACK
PRINT 'CHANGE DENAYED'
END

USE triger_fk_cursor
GO

CREATE OR ALTER TRIGGER PHARMACY_iud
ON
pharmacy
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @icount int= (select COUNT(*) from inserted)
DECLARE @dcount int = (select count(*) from deleted)
DECLARE @type varchar(1)
set @type =
CASE
WHEN @icount >0 and @dcount >0 then 'U'
when @icount = 0 and @dcount >0 then 'D'
when @icount >0 and @dcount =0 then 'U'
end

IF @type = 'U' or @type='I'
begin
declare @idins int = (select count(*) from inserted
where street not in (select *from street))

if
@idins >0 
begin
rollback
print 'Incorect foreign key1'
end
end
if @type ='U'
begin 
if (select count(*) from employee where pharmacy_id in (select id from deleted ))>0
rollback
print 'Statement has been terminated'
end
if
@type='D'
begin
if (Select count(*) from employee where pharmacy_id in (select id from deleted))>0
print 'The statementhas been terminated'
rollback
end
end
go

use triger_fk_cursor
CREATE OR ALTER TRIGGER post_UD
on 
post
after delete, update
as
IF @@ROWCOUNT = 0 return
BEGIN
DECLARE @del int = (SELECT COUNT(*) from deleted)
DECLARE @up int = (SELECT COUNT(*) FROM inserted)
IF @up =0
DELETE employee
WHERE post =(select *FROM deleted)
ELSE
UPDATE employee
SET post = (select *FROM inserted)
WHERE post IN (select * FROM deleted)
END
GO


USE triger_fk_cursor
GO

CREATE OR ALTER TRIGGER STREET_ud
ON 
street
AFTER DELETE, UPDATE
AS
IF @@ROWCOUNT = 0 RETURN
BEGIN
DECLARE @del int = (select count(*) from deleted)
DECLARE @UP INT = (SELECT COUNT (*) FROM inserted)
IF
@up = 0
delete pharmacy
where street in (select* from deleted)
else
update pharmacy
set street = (select *from inserted)
where street in (select *from deleted)
end
go

use triger_fk_cursor 
create or alter trigger zone_UD
on
zone
AFTER DELETE, UPDATE
AS
IF @@ROWCOUNT=0 RETURN
BEGIN
DECLARE @del int = (select COUNT(*) from deleted)
DECLARE @up INT = (SELECT COUNT(*) FROM inserted)
IF
@del>0
IF (SELECT COUNT(*) FROM medicine_zone WHERE zone_id in (SELECT id from deleted))>0
print 'The statement has been terminated'
rollback
end





