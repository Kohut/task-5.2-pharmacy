use triger_fk_cursor
go

declare @id int
declare @surname varchar (30)
declare @name char(30)
declare @middle_name varchar(30)
declare @identity_number char (10)
declare @passport char(10)
declare @experience decimal (10,1)
declare @birthday datetime
declare @post varchar (15)
declare @pharmacy_id int
declare @datetime datetime
declare @count int
declare @create nvarchar (max) = 'create table name_tabs.employee'
declare @sql nvarchar (max) = '(
id int , 
surname varchar(30) not null,
name char(30) not null,
middle_name varchar (30) not null,
identity_number char(10),
passport char(10),
experience decimal (10,1),
birthday datetime,
post varchar(15),
pharmacy_id int,
time_stamp datetime
)'
declare @table1 nvarchar(max)
declare @table2 nvarchar (max)
declare table_copy cursor for
select distinct*from employee
open table_copy
drop table if exists name_table.employee_left
drop table if exists name_table.employee_right

set @table1 = CONCAT(@create, '_left', @sql)
set @table2 = CONCAT(@create, '_right',@sql)
print @table1
execute sp_executesql @table1
execute sp_executesql @table2
fetch next from table_copy into @id, @surname,@name,@middle_name,@identity_number, @passport, @experience, @birthday, @post, @pharmacy_id
while @@FETCH_STATUS=0
begin
set @datetime = getdate()
set @count = ROUND(((rand()*10)+1),0)
if @count <5
begin
insert into name_tabs.employee_left select @id, @surname,@name,@middle_name,@identity_number, @passport, @experience, @birthday, @post, @pharmacy_id, @datetime
end
else
begin
insert into name_tabs.employee_right select @id, @surname,@name,@middle_name,@identity_number, @passport, @experience, @birthday, @post, @pharmacy_id, @datetime
end
fetch next from table_copy into @id, @surname,@name,@middle_name,@identity_number, @passport, @experience, @birthday, @post, @pharmacy_id
end
close table_copy
go
deallocate table_copy

go






